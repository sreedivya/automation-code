package com.avenuecode.qa.test.TestCases;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import com.avenuecode.qa.test.Pages.LogIn;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UseCases {
	public static void main(String[] args) throws InterruptedException {
		//Invoke chrome driver and login 
		System.setProperty("webdriver.chrome.driver", "F:\\Cache Downloads\\ScannedPP\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
	
		//login method is called
		
		LogIn login = new LogIn();
		login.UserLogin(driver);
		driver.findElement(By.xpath("//ul[@class='nav navbar-nav']//a[text()='My Tasks']")).click();
		driver.findElement(By.id("new_task")).click();
		WebDriverWait wait =  new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("new_task")));
		String displaymessage = driver.findElement(By.xpath("//div[@class='container']/h1")).getText();
		String Username = "Divya";
		
		//user logged message comparions method called
		
		login.displaymessagecomparision(displaymessage, Username);
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		driver.findElement(By.id("new_task")).sendKeys(timeStamp);
		driver.findElement(By.xpath("//span[@class='input-group-addon glyphicon glyphicon-plus']")).click();
		
		// Table code  - pull the data to a table and verify the element added is present
		List<WebElement> elements = driver.findElements(By.xpath("//div[@class='panel panel-default']//table[@class='table']"));
		
		for(int i=0;i<=elements.size()-1;i++)
		{
			String s  = elements.get(i).getText();
			if (s.contains(timeStamp) )	
			{
				System.out.println(timeStamp + "Entry is added");
				elements.get(i).click();
				elements.get(i).findElement(By.xpath("//button[text()='(0) Manage Subtasks']")).click();
				Thread.sleep(9000);
				break;
				
				
			}
			else
			{
				System.out.println("Entry is not added");
			}
		}
		
		//code to add sub task and verify added sub task
		
				driver.findElement(By.id("new_sub_task")).sendKeys(timeStamp);
				driver.findElement(By.id("dueDate")).sendKeys("10/10/2016");
				driver.findElement(By.id("add-subtask")).click();
				
				List<WebElement> subelements = driver.findElements(By.xpath("//div[@class='modal-body ng-scope']//table[@class='table']"));
				for(int i=0;i<=subelements.size()-1;i++)
				{
					String s  = subelements.get(i).getText();
					if (s.contains(timeStamp) )	
					{
						System.out.println(timeStamp + "Entry is added");											
						break;					
					
					}
					else
					{
						System.out.println("Entry is not added");
					}
				}
			driver.findElement(By.xpath("//button[text()='Close']")).click();
			Thread.sleep(9000);			
		
		driver.close();
		driver.quit();
	}
}
